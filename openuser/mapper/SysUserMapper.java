package com.zzkx.mall.openuser.mapper;

import com.zzkx.mall.openuser.entity.SysUserEntity;

public interface SysUserMapper {

    /**
     * 根据用户id,更新用户信息
     * @param sysUserEntity
     */
    void updateByPrimaryKeySelective(SysUserEntity sysUserEntity);

    /**
     * 新增用户信息
     * @param sysUserEntity
     */
    void insertSelective(SysUserEntity sysUserEntity);
}
